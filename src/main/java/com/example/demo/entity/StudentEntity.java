package com.example.demo.entity;

import lombok.Data;

import java.time.LocalDate;
import javax.persistence.*;

@Data
@Entity
@Table(name = "Student")
public class StudentEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;
    private String institute;
    private LocalDate birthdate;
}
