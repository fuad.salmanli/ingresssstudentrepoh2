package com.example.demo.controller;

import com.example.demo.entity.StudentEntity;
import com.example.demo.repository.StudentRepository;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController()
@RequestMapping("/student")
@AllArgsConstructor
public class StudentController {

    final StudentRepository studentRepository;

    @PutMapping("/add")
    public ResponseEntity addStudent(@NotNull String name, @NotNull String institute, @NotNull LocalDate birthDate){
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setName(name);
        studentEntity.setInstitute(institute);
        studentEntity.setBirthdate(birthDate);
        studentEntity = studentRepository.save(studentEntity);
        return ResponseEntity.ok(studentEntity.getId());
    }

    @GetMapping("/get-by-name")
    public ResponseEntity<List<StudentEntity>> getByName(String name){
        var lstStudents = studentRepository.findByName(name);
        if (lstStudents == null || lstStudents.isEmpty())
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(lstStudents);
    }
}
