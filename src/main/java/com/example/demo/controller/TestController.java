package com.example.demo.controller;

import com.example.demo.entity.StudentEntity;
import com.example.demo.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController()
@RequestMapping("/test")
@AllArgsConstructor
public class TestController {

    @GetMapping("test-me")
    public ResponseEntity<String> testMe(String name){
        return ResponseEntity.ok("Hello " + name);
    }
}
